package com.khasni.fa13.backend

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class GameDetailsServiceTest {

    private val mockFetcher : GameDetailsService.GameDetailsFetcher = mock()

    private val gameDetailsService = GameDetailsService(mockFetcher)

    @Test
    fun loadVideoData_parsesRawHtml() {
        whenever(mockFetcher.getVideoData(12345))
                .thenReturn("/data/game-video-data.html".readResource())

        assertThat(gameDetailsService.loadVideoData(12345))
                .isEqualToComparingFieldByFieldRecursively(GameVideoData(
                        info = GameVideoData.Info(
                                hostsName = "Host Team",
                                guestsName = "Guest Team",
                                hostsSet = "Host Team M-4-4-2 баланс фланг жёстко прессинг 4.12 (Host Manager)",
                                guestsSet = "Guest Team M-4-4-2 баланс фланг нормально персональная опека 2.20 (Guest Manager)",
                                hosts = arrayOf(
                                        GameVideoData.Player(
                                                name = "Host Player 1",
                                                number = "5",
                                                power = 63.1f,
                                                pos = "ВР",
                                                stat = ""
                                        ),
                                        GameVideoData.Player(
                                                name = "Host Player 2",
                                                number = "11",
                                                power = 58.9f,
                                                pos = "ЦП",
                                                stat = "дпт!!!"
                                        )
                                ),
                                guests = arrayOf(
                                        GameVideoData.Player(
                                                name = "Guest Player 1",
                                                number = "1",
                                                power = 38.5f,
                                                pos = "ВР",
                                                stat = ""
                                        ),
                                        GameVideoData.Player(
                                                name = "Guest Player 2",
                                                number = "24",
                                                power = 41.6f,
                                                pos = "ЛЗ",
                                                stat = "п"
                                        )
                                ),
                                stad = "Host Stadium (35802)",
                                stat = arrayOf(
                                        arrayOf("0 (0)","stat 1","0 (0)"),
                                        arrayOf("7 (5), 71%","stat 2","4 (2), 50%")
                                )
                        ),
                        moments = arrayOf(
                                GameVideoData.Moment(
                                        code = 100,
                                        firstarg = 0,
                                        secondarg = 0,
                                        hosts = arrayOf(
                                                GameVideoData.Position(-570, 0),
                                                GameVideoData.Position(0, 110)
                                        ),
                                        guests = arrayOf(
                                                GameVideoData.Position(580, 10),
                                                GameVideoData.Position(420, 240)
                                        ),
                                        ball = GameVideoData.Position(0, 0)
                                ),
                                GameVideoData.Moment(
                                        code = 114,
                                        firstarg = 0,
                                        secondarg = 0,
                                        hosts = emptyArray(),
                                        guests = emptyArray(),
                                        ball = null
                                )
                        ),
                        time = intArrayOf(100, 0)
                ))
    }

    fun String.readResource(): String {
        return this.javaClass::class.java.getResource(this).readText()
    }
}