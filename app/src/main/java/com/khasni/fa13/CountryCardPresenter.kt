package com.khasni.fa13

import android.support.v17.leanback.widget.Presenter
import android.util.Log
import android.view.ViewGroup
import com.bumptech.glide.Glide

/**
 * A CountryCardPresenter is used to generate Views and bind Objects to them on demand.
 * It contains an CountryCardView.
 */
class CountryCardPresenter : Presenter() {

    companion object {
        private const val TAG = "CountryCardPresenter"
    }

    override fun onCreateViewHolder(parent: ViewGroup): Presenter.ViewHolder {
        Log.d(TAG, "onCreateViewHolder")

        val cardView = createGameCardView(parent)
        return Presenter.ViewHolder(cardView)
    }

    private fun createGameCardView(parent: ViewGroup): CountryCardView {
        val view = CountryCardView(parent.context, null)
        view.isFocusable = true
        view.isFocusableInTouchMode = true
        return view
    }

    override fun onBindViewHolder(viewHolder: Presenter.ViewHolder, item: Any) {
        val country = item as Country
        Log.d(TAG, "onBindViewHolder: $country")

        val cardView = viewHolder.view as CountryCardView

        Glide.with(cardView.context)
                .load(country.countryLogoUrl)
                .into(cardView.countryLogo)
    }

    override fun onUnbindViewHolder(viewHolder: Presenter.ViewHolder) {
        Log.d(TAG, "onUnbindViewHolder")

        val cardView = viewHolder.view as CountryCardView
        cardView.clear()
    }
}
