package com.khasni.fa13

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout

/**
 *
 */
open class GamePlaybackView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    init {
        val inflater = LayoutInflater.from(getContext())
        inflater.inflate(R.layout.view_game_playback, this)
    }
}
