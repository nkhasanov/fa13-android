package com.khasni.fa13

import com.khasni.fa13.backend.tournamentLogoImage
import java.io.Serializable

data class Country(
        var id: String,
        var title: String
) : Serializable {

    companion object {
        internal const val serialVersionUID = 727566175075960653L
    }

    val countryLogoUrl: String
        get() {
            return tournamentLogoImage("r$id")
        }

    override fun toString(): String {
        return "Game(" +
                "id=$id, " +
                "title=$title" +
                ")"
    }

}
