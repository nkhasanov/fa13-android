package com.khasni.fa13.backend

import okhttp3.OkHttpClient
import okhttp3.Request

open class BackendClient: GameDetailsService.GameDetailsFetcher {
    var http = OkHttpClient()

    override fun getVideoData(gameId: Int): String? {
        val request = Request.Builder()
                .url(gameVideoData(gameId))
                .build()

        val response = http.newCall(request).execute()
        return response.body()?.string()
    }
}