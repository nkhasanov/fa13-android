package com.khasni.fa13.backend

fun tournamentLogoImage(id: String): String {
    return "http://repository.fa13.info/site/tourn/$id.png"
}

fun teamLogoImage(id: String): String {
    return "http://repository.fa13.info/site/team/$id.png"
}

fun gameVideoData(id: Int): String {
    return "http://old.fa13.info/video/data.php?video=$id"
}

