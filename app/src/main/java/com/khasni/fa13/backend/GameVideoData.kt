package com.khasni.fa13.backend

import java.io.Serializable
import java.util.*

class GameVideoData(
        var info: Info,
        var moments: Array<Moment>,
        var time: IntArray
) : Serializable {
    override fun toString(): String {
        return "GameVideoData(" +
                "info=$info, " +
                "moments=$moments, " +
                "time=$time" +
                ")"
    }

    class Info(
            var hostsName: String,
            var guestsName: String,
            var hostsSet: String,
            var guestsSet: String,
            var hosts: Array<Player>,
            var guests: Array<Player>,
            var stad: String,
            var stat: Array<Array<String>>
    ) : Serializable {
        override fun toString(): String {
            return "Info(" +
                    "hostsName='$hostsName', " +
                    "guestsName='$guestsName', " +
                    "hostsSet='$hostsSet', " +
                    "guestsSet='$guestsSet', " +
                    "hosts=${Arrays.toString(hosts)}, " +
                    "guests=${Arrays.toString(guests)}, " +
                    "stad='$stad', " +
                    "stat=${Arrays.toString(stat)}" +
                    ")"
        }
    }

    class Player(
            var name: String,
            var number: String,
            var power: Float,
            var pos: String,
            var stat: String
    ) : Serializable {
        override fun toString(): String {
            return "Player(" +
                    "name='$name'," +
                    "number='$number'," +
                    "power=$power," +
                    "pos='$pos'," +
                    "stat='$stat'" +
                    ")"
        }
    }

    class Moment(
            var code: Int,
            var firstarg: Int,
            var secondarg: Int,
            var hosts: Array<Position>,
            var guests: Array<Position>,
            var ball: Position?
    ) : Serializable {
        override fun toString(): String {
            return "Moment(" +
                    "code=$code," +
                    "firstarg=$firstarg," +
                    "secondarg=$secondarg," +
                    "hosts=${Arrays.toString(hosts)}," +
                    "guests=${Arrays.toString(guests)}," +
                    "ball=$ball" +
                    ")"
        }
    }

    class Position(
            var x: Int,
            var y: Int
    ) : Serializable {
        override fun toString(): String {
            return "Position(x=$x, y=$y)"
        }
    }
}
