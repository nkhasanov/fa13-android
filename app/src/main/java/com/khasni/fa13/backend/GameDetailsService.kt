package com.khasni.fa13.backend

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import org.jsoup.Jsoup

class GameDetailsService(private val fetcher : GameDetailsFetcher) {
    interface GameDetailsFetcher {
        fun getVideoData(gameId: Int): String?
    }

    private val gson = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()

    fun loadVideoData(gameId: Int): GameVideoData {
        val responseBody = fetcher.getVideoData(gameId)

        val fragment = Jsoup.parseBodyFragment(responseBody)
                .getElementsByTag("script")
                .html()

        val jsonData = fragment.substring(
                fragment.indexOf('{'),
                fragment.lastIndexOf('}') + 1
        )

        return gson.fromJson<GameVideoData>(jsonData, GameVideoData::class.java)
    }
}