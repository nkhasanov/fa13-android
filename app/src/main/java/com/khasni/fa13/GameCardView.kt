package com.khasni.fa13

import android.content.Context
import android.support.v17.leanback.widget.BaseCardView
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView

/**
 *
 */
open class GameCardView(context: Context, attrs: AttributeSet?, defStyle: Int = 0) : BaseCardView(context, attrs, defStyle) {
    val tournamentLogo: ImageView
    val firstTeamTitleView: TextView
    val firstTeamLogoView: ImageView
    val secondTeamTitleView: TextView
    val secondTeamLogoView: ImageView

    init {
        val inflater = LayoutInflater.from(getContext())
        inflater.inflate(R.layout.view_game_card, this)

        tournamentLogo = findViewById<ImageView>(R.id.tournament_logo)
        firstTeamTitleView = findViewById<TextView>(R.id.first_team_title)
        firstTeamLogoView = findViewById<ImageView>(R.id.first_team_logo)
        secondTeamTitleView = findViewById<TextView>(R.id.second_team_title)
        secondTeamLogoView = findViewById<ImageView>(R.id.second_team_logo)
    }

    fun setFirstTeamTitle(title: String) {
        firstTeamTitleView.text = title
    }

    fun setSecondTeamTitle(title: String) {
        secondTeamTitleView.text = title
    }

    fun clear() {
        tournamentLogo.setImageDrawable(null)
        firstTeamLogoView.setImageDrawable(null)
        secondTeamLogoView.setImageDrawable(null)
    }
}
