package com.khasni.fa13

import android.support.v17.leanback.widget.Presenter
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlin.properties.Delegates

/**
 * A GameCardPresenter is used to generate Views and bind Objects to them on demand.
 * It contains an GameCardView.
 */
class GameCardPresenter : Presenter() {

    companion object {
        private const val TAG = "GameCardPresenter"
    }

    private var sSelectedBackgroundColor: Int by Delegates.notNull()
    private var sDefaultBackgroundColor: Int by Delegates.notNull()

    override fun onCreateViewHolder(parent: ViewGroup): Presenter.ViewHolder {
        Log.d(TAG, "onCreateViewHolder")

        sDefaultBackgroundColor = ContextCompat.getColor(parent.context, R.color.default_background)
        sSelectedBackgroundColor = ContextCompat.getColor(parent.context, R.color.selected_background)

        val cardView = createGameCardView(parent)
        return Presenter.ViewHolder(cardView)
    }

    private fun createGameCardView(parent: ViewGroup): GameCardView {
        val view = object : GameCardView(parent.context, null) {
            override fun setSelected(selected: Boolean) {
                updateCardBackgroundColor(this, selected)
                super.setSelected(selected)
            }
        }
        view.isFocusable = true
        view.isFocusableInTouchMode = true
        updateCardBackgroundColor(view, false)
        return view
    }

    private fun updateCardBackgroundColor(view: GameCardView, selected: Boolean) {
        val color = if (selected) sSelectedBackgroundColor else sDefaultBackgroundColor
        view.setBackgroundColor(color)
//        view.setInfoAreaBackgroundColor(color)
    }

    override fun onBindViewHolder(viewHolder: Presenter.ViewHolder, item: Any) {
        val game = item as Game
        Log.d(TAG, "onBindViewHolder: $game")

        val cardView = viewHolder.view as GameCardView
        cardView.setFirstTeamTitle(game.teamHome.title)
        cardView.setSecondTeamTitle(game.teamGuest.title)

        Glide.with(cardView.context)
                .load(game.tournamentLogoUrl)
                .into(cardView.tournamentLogo)

        Glide.with(cardView.context)
                .load(game.teamHome.teamLogoUrl)
                .into(cardView.firstTeamLogoView)

        Glide.with(cardView.context)
                .load(game.teamGuest.teamLogoUrl)
                .into(cardView.secondTeamLogoView)
    }

    override fun onUnbindViewHolder(viewHolder: Presenter.ViewHolder) {
        Log.d(TAG, "onUnbindViewHolder")

        val cardView = viewHolder.view as GameCardView
        cardView.clear()
    }
}
