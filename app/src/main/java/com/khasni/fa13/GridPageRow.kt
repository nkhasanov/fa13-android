package com.khasni.fa13

import android.support.v17.leanback.widget.ArrayObjectAdapter
import android.support.v17.leanback.widget.HeaderItem
import android.support.v17.leanback.widget.PageRow

class GridPageRow(headerItem: HeaderItem?, val adapter: ArrayObjectAdapter) : PageRow(headerItem) {

}