package com.khasni.fa13

import android.support.v17.leanback.app.BrowseFragment
import android.support.v17.leanback.widget.OnItemViewClickedListener

class CategoryPageRowFragmentFactory(val itemViewClickedListener: OnItemViewClickedListener) : BrowseFragment.FragmentFactory<CategoryFragment>() {

    override fun createFragment(row: Any?): CategoryFragment {
        val categoryRow = row as CategoryPageRow
        val fragment = CategoryFragment()
        fragment.adapter = categoryRow.adapter
        fragment.onItemViewClickedListener = itemViewClickedListener
        return fragment
    }
}