package com.khasni.fa13

import com.khasni.fa13.backend.tournamentLogoImage
import java.io.Serializable

data class Game(
        var id: Long,
        var tournamentId: String,
        var teamHome: Team,
        var teamGuest: Team,
        var cardBackgroundImageUrl: String? = null,
        var backgroundImageUrl: String? = null
) : Serializable {

    companion object {
        internal const val serialVersionUID = 727566175075960653L
    }

    val title: String
        get() {
            return "${teamHome.title} - ${teamGuest.title}"
        }

    val tournamentLogoUrl: String
        get() {
            return tournamentLogoImage(tournamentId)
        }

    override fun toString(): String {
        return "Game(" +
                "id=$id, " +
                "tournamentId=$tournamentId, " +
                "teamHome=$teamHome, " +
                "teamGuest=$teamGuest, " +
                "cardBackgroundImageUrl=$cardBackgroundImageUrl, " +
                "backgroundImageUrl=$backgroundImageUrl" +
                ")"
    }

}
