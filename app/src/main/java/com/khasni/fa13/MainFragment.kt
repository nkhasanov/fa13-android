package com.khasni.fa13

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v17.leanback.app.BrowseFragment
import android.support.v17.leanback.widget.*
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast

/**
 * Loads a grid of cards with movies to browse.
 */
class MainFragment : BrowseFragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        Log.i(TAG, "onCreate")
        super.onActivityCreated(savedInstanceState)

        setupUIElements()

        loadRows()

        setupEventListeners()
    }

    private fun setupUIElements() {
        title = getString(R.string.browse_title)
        // over title
        headersState = BrowseFragment.HEADERS_ENABLED
        isHeadersTransitionOnBackEnabled = true

        // set fastLane (or headers) background color
        brandColor = ContextCompat.getColor(context, R.color.fastlane_background)
        // set search icon color
        searchAffordanceColor = ContextCompat.getColor(context, R.color.search_opaque)
    }

    private fun loadRows() {
        mainFragmentRegistry.registerFragment(CategoryPageRow::class.java, CategoryPageRowFragmentFactory(ItemViewClickedListener()))
        mainFragmentRegistry.registerFragment(GridPageRow::class.java, GridPageRowFragmentFactory())

        val cardPresenter = GameCardPresenter()

        adapter = newArrayObjectAdapter(ListRowPresenter(),

                GridPageRow(
                        HeaderItem(getString(R.string.nav_regular_championship)),
                        newArrayObjectAdapter(CountryCardPresenter(),
                                CountryList.list
                        )
                ),

                CategoryPageRow(
                        HeaderItem(getString(R.string.nav_champions_league)),
                        newArrayObjectAdapter(ListRowPresenter(),
                                ListRow(
                                        HeaderItem("Последние игры"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("2 тур"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("1 тур"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                )
                        )
                ),
                CategoryPageRow(
                        HeaderItem(getString(R.string.nav_association_cup)),
                        newArrayObjectAdapter(ListRowPresenter(),
                                ListRow(
                                        HeaderItem("Последние игры"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("2 тур"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("1 тур"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                )
                        )
                ),
                CategoryPageRow(
                        HeaderItem(getString(R.string.nav_federations_cup)),
                        newArrayObjectAdapter(ListRowPresenter(),
                                ListRow(
                                        HeaderItem("Последние игры"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("2 тур"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("1 тур"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                )
                        )
                ),
                CategoryPageRow(
                        HeaderItem(getString(R.string.nav_world_cup)),
                        newArrayObjectAdapter(ListRowPresenter(),
                                ListRow(
                                        HeaderItem("Последние игры"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("2 тур"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("1 тур"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                )
                        )
                ),
                CategoryPageRow(
                        HeaderItem(getString(R.string.nav_youth_world_cup)),
                        newArrayObjectAdapter(ListRowPresenter(),
                                ListRow(
                                        HeaderItem("Последние игры"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("2 тур"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("1 тур"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                )
                        )
                ),
                CategoryPageRow(
                        HeaderItem(getString(R.string.nav_commercial_cups)),
                        newArrayObjectAdapter(ListRowPresenter(),
                                ListRow(
                                        HeaderItem("Последние игры"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("2 тур"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("1 тур"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                )
                        )
                ),
                CategoryPageRow(
                        HeaderItem(getString(R.string.nav_friendly_games)),
                        newArrayObjectAdapter(ListRowPresenter(),
                                ListRow(
                                        HeaderItem("04.02.2018"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("02.02.2018"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("01.02.2018"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("31.01.2018"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("28.01.2018"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("26.01.2018"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                ),
                                ListRow(
                                        HeaderItem("22.01.2018"),
                                        newArrayObjectAdapter(cardPresenter, GameList.list)
                                )
                        )
                ),

                CategoryPageRow(
                        HeaderItem(getString(R.string.nav_preferences)),
                        newArrayObjectAdapter(ListRowPresenter(),
                                ListRow(
                                        HeaderItem(getString(R.string.nav_preferences)),
                                        newArrayObjectAdapter(GridItemPresenter(),
                                                getString(R.string.grid_view),
                                                getString(R.string.error_fragment),
                                                getString(R.string.personal_settings)
                                        )
                                )
                        )
                )
        )
    }



    private fun setupEventListeners() {
        setOnSearchClickedListener {
            Toast.makeText(context, "Implement your own in-app search", Toast.LENGTH_LONG)
                    .show()
        }

        onItemViewSelectedListener = ItemViewSelectedListener()
    }

    private inner class ItemViewClickedListener : OnItemViewClickedListener {
        override fun onItemClicked(itemViewHolder: Presenter.ViewHolder, item: Any,
                                   rowViewHolder: RowPresenter.ViewHolder, row: Row) {

            Log.d(TAG, "onItemClicked, Item: " + item)
            if (item is Game) {
                val intent = Intent(context, DetailsActivity::class.java)
                intent.putExtra(DetailsActivity.GAME, item)

                // todo: this makes transition to detailed view animated
//                val bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(
//                        activity,
//                        (itemViewHolder.view as ImageCardView).mainImageView,
//                        DetailsActivity.SHARED_ELEMENT_NAME).toBundle()
                activity.startActivity(intent/*, bundle*/)
            } else if (item is String) {
                if (item.contains(getString(R.string.error_fragment))) {
                    val intent = Intent(context, BrowseErrorActivity::class.java)
                    startActivity(intent)
                } else {
                    Toast.makeText(context, item, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private inner class ItemViewSelectedListener : OnItemViewSelectedListener {
        override fun onItemSelected(itemViewHolder: Presenter.ViewHolder?, item: Any?,
                                    rowViewHolder: RowPresenter.ViewHolder, row: Row) {
        }
    }

    private inner class GridItemPresenter : Presenter() {
        override fun onCreateViewHolder(parent: ViewGroup): Presenter.ViewHolder {
            val view = TextView(parent.context)
            view.layoutParams = ViewGroup.LayoutParams(GRID_ITEM_WIDTH, GRID_ITEM_HEIGHT)
            view.isFocusable = true
            view.isFocusableInTouchMode = true
            view.setBackgroundColor(ContextCompat.getColor(context, R.color.default_background))
            view.setTextColor(Color.WHITE)
            view.gravity = Gravity.CENTER
            return Presenter.ViewHolder(view)
        }

        override fun onBindViewHolder(viewHolder: Presenter.ViewHolder, item: Any) {
            (viewHolder.view as TextView).text = item as String
        }

        override fun onUnbindViewHolder(viewHolder: Presenter.ViewHolder) {}
    }

    companion object {
        private val TAG = "MainFragment"

        private val GRID_ITEM_WIDTH = 200
        private val GRID_ITEM_HEIGHT = 200
    }
}
