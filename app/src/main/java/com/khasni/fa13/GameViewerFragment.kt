/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.khasni.fa13

import android.app.Fragment
import android.content.Intent
import android.os.Bundle
import android.support.v17.leanback.widget.*
import android.support.v4.app.ActivityOptionsCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide

/**
 * A wrapper fragment for leanback details screens.
 * It shows a detailed view of video and its metadata plus related videos.
 */
class GameViewerFragment : Fragment() {

    private var mSelectedGame: Game? = null

//    private lateinit var mPresenterSelector: ClassPresenterSelector
//    private lateinit var mAdapter: ArrayObjectAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        Log.d(TAG, "onCreateView DetailsFragment")

        return inflater.inflate(R.layout.fragment_game_viewer, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val game = activity.intent.getSerializableExtra(DetailsActivity.GAME) as Game

        Glide.with(context)
                .load(game.tournamentLogoUrl)
                .into(view.findViewById(R.id.tournament_logo))
        Glide.with(context)
                .load(game.teamHome.teamLogoUrl)
                .into(view.findViewById(R.id.first_team_logo))
        Glide.with(context)
                .load(game.teamGuest.teamLogoUrl)
                .into(view.findViewById(R.id.second_team_logo))

        val firstTeamTitle: TextView = view.findViewById(R.id.first_team_title)
        firstTeamTitle.text = game.teamHome.title
        val secondTeamTitle: TextView = view.findViewById(R.id.second_team_title)
        secondTeamTitle.text = game.teamGuest.title

//        mPresenterSelector = ClassPresenterSelector()
//        mAdapter = ArrayObjectAdapter(mPresenterSelector)
//        setupDetailsOverviewRow()
//        setupDetailsOverviewRowPresenter()
//        setupRelatedMovieListRow()
//        adapter = mAdapter
//        initializeBackground(mSelectedGame)
//        onItemViewClickedListener = ItemViewClickedListener()
    }

//    private fun initializeBackground(game: Game?) {
//        Glide.with(context)
//                .load(game?.backgroundImageUrl)
//                .centerCrop()
//                .fallback(R.drawable.game_playback_background)
//                .error(R.drawable.game_playback_background)
//                .into<SimpleTarget<GlideDrawable>>(object: SimpleTarget<GlideDrawable>() {
//                    override fun onResourceReady(resource: GlideDrawable?, glideAnimation: GlideAnimation<in GlideDrawable>?) {
//                        val layout = view.findViewById<RelativeLayout>(R.id.view_layout)
//                        layout.background = resource
//                    }
//
//                })
//    }

//    private fun setupDetailsOverviewRow() {
//        Log.d(TAG, "doInBackground: " + mSelectedGame?.toString())
//        val row = DetailsOverviewRow(mSelectedGame)
//        row.imageDrawable = ContextCompat.getDrawable(context, R.drawable.default_background)
//        val width = convertDpToPixel(context, DETAIL_THUMB_WIDTH)
//        val height = convertDpToPixel(context, DETAIL_THUMB_HEIGHT)
//        Glide.with(context)
//                .load(mSelectedGame?.backgroundImageUrl)
//                .centerCrop()
//                .error(R.drawable.default_background)
//                .into<SimpleTarget<GlideDrawable>>(object : SimpleTarget<GlideDrawable>(width, height) {
//                    override fun onResourceReady(resource: GlideDrawable,
//                                                 glideAnimation: GlideAnimation<in GlideDrawable>) {
//                        Log.d(TAG, "details overview card image url ready: " + resource)
//                        row.imageDrawable = resource
//                        mAdapter.notifyArrayItemRangeChanged(0, mAdapter.size())
//                    }
//                })
//
//        val actionAdapter = ArrayObjectAdapter()
//
//        actionAdapter.add(
//                Action(
//                        ACTION_WATCH_TRAILER,
//                        resources.getString(R.string.watch_trailer_1),
//                        resources.getString(R.string.watch_trailer_2)))
//        actionAdapter.add(
//                Action(
//                        ACTION_RENT,
//                        resources.getString(R.string.rent_1),
//                        resources.getString(R.string.rent_2)))
//        actionAdapter.add(
//                Action(
//                        ACTION_BUY,
//                        resources.getString(R.string.buy_1),
//                        resources.getString(R.string.buy_2)))
//        row.actionsAdapter = actionAdapter
//
//        mAdapter.add(row)
//    }

//    private fun setupDetailsOverviewRowPresenter() {
//        // Set detail background.
//        val detailsPresenter = FullWidthDetailsOverviewRowPresenter(DetailsDescriptionPresenter())
//        detailsPresenter.backgroundColor =
//                ContextCompat.getColor(context, R.color.selected_background)
//
//        // Hook up transition element.
//        val sharedElementHelper = FullWidthDetailsOverviewSharedElementHelper()
//        sharedElementHelper.setSharedElementEnterTransition(
//                activity, DetailsActivity.SHARED_ELEMENT_NAME)
//        detailsPresenter.setListener(sharedElementHelper)
//        detailsPresenter.isParticipatingEntranceTransition = true
//
//        detailsPresenter.onActionClickedListener = OnActionClickedListener { action ->
//            if (action.id == ACTION_WATCH_TRAILER) {
//                val intent = Intent(context, PlaybackActivity::class.java)
//                intent.putExtra(DetailsActivity.GAME, mSelectedGame)
//                startActivity(intent)
//            } else {
//                Toast.makeText(context, action.toString(), Toast.LENGTH_SHORT).show()
//            }
//        }
//        mPresenterSelector.addClassPresenter(DetailsOverviewRow::class.java, detailsPresenter)
//    }

//    private fun setupRelatedMovieListRow() {
//        val subcategories = arrayOf(getString(R.string.related_movies))
//        val list = GameList.list
//
//        Collections.shuffle(list)
//        val listRowAdapter = ArrayObjectAdapter(GameCardPresenter())
//        for (j in 0 until NUM_COLS) {
//            listRowAdapter.add(list[j % 5])
//        }
//
//        val header = HeaderItem(0, subcategories[0])
//        mAdapter.add(ListRow(header, listRowAdapter))
//        mPresenterSelector.addClassPresenter(ListRow::class.java, ListRowPresenter())
//    }

//    fun convertDpToPixel(context: Context, dp: Int): Int {
//        val density = context.applicationContext.resources.displayMetrics.density
//        return Math.round(dp.toFloat() * density)
//    }

    private inner class ItemViewClickedListener : OnItemViewClickedListener {
        override fun onItemClicked(itemViewHolder: Presenter.ViewHolder?, item: Any?,
                                   rowViewHolder: RowPresenter.ViewHolder, row: Row) {
            if (item is Game) {
                Log.d(TAG, "Item: " + item.toString())
                val intent = Intent(context, DetailsActivity::class.java)
                intent.putExtra(resources.getString(R.string.movie), mSelectedGame)

                val bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        activity,
                        (itemViewHolder?.view as ImageCardView).mainImageView,
                        DetailsActivity.SHARED_ELEMENT_NAME).toBundle()
                activity.startActivity(intent, bundle)
            }
        }
    }

    companion object {
        private val TAG = "GameViewerFragment"

        private val ACTION_WATCH_TRAILER = 1L
        private val ACTION_RENT = 2L
        private val ACTION_BUY = 3L

        private val DETAIL_THUMB_WIDTH = 274
        private val DETAIL_THUMB_HEIGHT = 274

        private val NUM_COLS = 10
    }
}