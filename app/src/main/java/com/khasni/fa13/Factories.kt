package com.khasni.fa13

import android.support.v17.leanback.widget.ArrayObjectAdapter
import android.support.v17.leanback.widget.Presenter

fun newArrayObjectAdapter(presenter: Presenter, vararg items: Any): ArrayObjectAdapter {
    val clWeek1Adapter = ArrayObjectAdapter(presenter)
    for (m in items) {
        clWeek1Adapter.add(m)
    }
    return clWeek1Adapter
}

fun newArrayObjectAdapter(presenter: Presenter, items: List<Any>): ArrayObjectAdapter {
    return newArrayObjectAdapter(presenter, *items.toTypedArray())
}
