package com.khasni.fa13

import android.support.v17.leanback.app.BrowseFragment
import android.support.v17.leanback.app.VerticalGridFragment
import android.support.v17.leanback.widget.VerticalGridPresenter

class GridFragment : VerticalGridFragment(), BrowseFragment.MainFragmentAdapterProvider {

    private val mMainFragmentAdapter: MainFragmentAdapter = MainFragmentAdapter(this)

    init {
        gridPresenter = VerticalGridPresenter()
        gridPresenter.numberOfColumns = 9
        gridPresenter.shadowEnabled = false
    }

    override fun getMainFragmentAdapter(): MainFragmentAdapter {
        return mMainFragmentAdapter
    }

    class MainFragmentAdapter(fragment: GridFragment) : BrowseFragment.MainFragmentAdapter<GridFragment>(fragment) {

        override fun onTransitionPrepare(): Boolean {
            fragment.onEntranceTransitionPrepare()
            return true
        }

        override fun onTransitionStart() {
            fragment.onEntranceTransitionStart()
        }

        override fun onTransitionEnd() {
            fragment.onEntranceTransitionEnd()
        }
    }
}