package com.khasni.fa13

object GameList {
    val list: List<Game> by lazy {
        arrayListOf(
                Game(
                        1,
                        "rfg",
                        Team("P34", "Пасуш де Феррейра"),
                        Team("Bsh", "Филадельфия Юнион")
                ),
                Game(
                        2,
                        "rfg",
                        Team("Lan", "Ланс"),
                        Team("B58", "Б-93")
                ),
                Game(
                        49965,
                        "rfg",
                        Team("Dnn", "Динамо (Мн)"),
                        Team("MnU", "Манчестер Юнайтед")
                ),
                Game(
                        49961,
                        "rfg",
                        Team("A20", "Атлетико Минейро"),
                        Team("K44", "Койлрейн")
                ),
                Game(
                        43050,
                        "rGu",
                        Team("K86", "Шукура"),
                        Team("D16", "Дила")
                )
        )
    }
}