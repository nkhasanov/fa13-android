package com.khasni.fa13

import android.support.v17.leanback.app.BrowseFragment
import android.support.v17.leanback.app.VerticalGridFragment

class GridPageRowFragmentFactory : BrowseFragment.FragmentFactory<VerticalGridFragment>() {

    override fun createFragment(row: Any?): VerticalGridFragment {
        val pageRow = row as GridPageRow
        val fragment = GridFragment()
        fragment.adapter = pageRow.adapter
        return fragment
    }
}