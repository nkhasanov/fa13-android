package com.khasni.fa13

import com.khasni.fa13.backend.teamLogoImage
import java.io.Serializable

data class Team(
        var id: String,
        var title: String
) : Serializable {

    companion object {
        internal const val serialVersionUID = 727566175075960653L
    }

    val teamLogoUrl: String
        get() {
            return teamLogoImage(id)
        }

    override fun toString(): String {
        return "Team(id=$id, title=$title)"
    }
}
