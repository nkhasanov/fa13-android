package com.khasni.fa13

import android.content.Context
import android.support.v17.leanback.widget.BaseCardView
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView

/**
 *
 */
open class CountryCardView(context: Context, attrs: AttributeSet?, defStyle: Int = 0) : BaseCardView(context, attrs, defStyle) {
    val countryLogo: ImageView

    init {
        val inflater = LayoutInflater.from(getContext())
        inflater.inflate(R.layout.view_country_card, this)

        countryLogo = findViewById<ImageView>(R.id.country_logo)
    }

    fun clear() {
        countryLogo.setImageDrawable(null)
    }
}
